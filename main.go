package main

import "github.com/gin-gonic/gin"

func main() {
	r := gin.Default()
	r.GET("/", Handle)
	r.Run(":8000")
}

func Handle(context *gin.Context)  {
	context.JSON(200, "Super-puper test!! Ok!!!")
}

