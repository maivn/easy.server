FROM golang:1.8

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...
RUN go get "github.com/gin-gonic/gin"

ENV PORT 8000
EXPOSE 8000

CMD ["app"]